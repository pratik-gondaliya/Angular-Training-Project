import { SampleAngularProjectPage } from './app.po';

describe('sample-angular-project App', () => {
  let page: SampleAngularProjectPage;

  beforeEach(() => {
    page = new SampleAngularProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
