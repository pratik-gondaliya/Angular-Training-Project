import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';

@Injectable()
export class TestService {

  constructor(private httpClient:HttpClient) { }

  public getRequest() {
    console.log('get request called');
    return this.httpClient.get("http://jsonplaceholder.typicode.com/posts/1");
  }

  sampleReturn() {
    return 'sample';
  }
}
