import { TestBed, inject,fakeAsync,flushMicrotasks,ComponentFixture } from '@angular/core/testing';
import { HttpClientModule} from '@angular/common/http';
import { TestService } from './test.service';

let service:TestService;


let httpResponse = {
  "userId": 1,
  "id": 1,
  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
};
describe('TestService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule],
      providers: [TestService]
    });
    this.service = TestBed.get(TestService);
  });



  it('should be created', () => {
    expect(this.service).toBeTruthy();
  });

  it('should return sample as avalue' , () =>{
      let val:any = this.service.sampleReturn();
      expect(val).toEqual('sample');
  });

  it('observable testing', (done) => {
      let res;
      res = this.service.getRequest().subscribe(data=>{
        expect(data).toEqual(httpResponse);   
        done();
      });
  });
});
