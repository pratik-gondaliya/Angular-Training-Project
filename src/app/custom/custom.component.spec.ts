import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomComponent } from './custom.component';

describe('CustomComponent', () => {
  let component: CustomComponent;
  let fixture: ComponentFixture<CustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create Custom Component', () => {
    expect(component).toBeTruthy();
  });

  it('If a is greater than 10', () => {
    expect(component.a).toBeGreaterThan(10);
  });

  it('increment button clicked should be increment by one', () => {
    let tempval = component.a + 1;
    component.inc();
    let temp2Val = component.a;
    expect(tempval).toEqual(temp2Val);
  });

  it('decrement button clicked should be decrement by one', () => {
    let tempval = component.a - 1;
    component.dec();
    let temp2Val = component.a;
    expect(tempval).toEqual(temp2Val);
  });

});
