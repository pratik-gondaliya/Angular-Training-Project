import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom',
  templateUrl: './custom.component.html',
  styleUrls: ['./custom.component.css']
})

export class CustomComponent implements OnInit {

  public a:any = 11;
  constructor() {
    
  }
  ngOnInit() {
  }

  inc() {
    this.a = this.a+1;
  }

  dec() {
    this.a = this.a-1;
  }
}
