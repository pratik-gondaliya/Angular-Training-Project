export interface EmployeeData{
    userId:number,
    id:number,
    title:string,
    body:string
}