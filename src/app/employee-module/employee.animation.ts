import { trigger, state, style, animate, transition,keyframes,group } from '@angular/animations';

export const fadeInAnimation =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('fadeInAnimation', [
 
        // route 'enter' transition
        transition(':enter', [
 
            // css styles at start of transition
            style({ opacity: 0 }),
 
            // animation and styles at end of transition
            animate('5s', style({ opacity: 1 }))
        ]),
    ]);


export const  heroState =  
    trigger('heroState', [
        state('inactive', style({
        backgroundColor: 'transparent',
        transform: 'scale(1)'
        })),
        state('active',   style({
        backgroundColor: '#ffff32',
        transform: 'scale(1.1)'
        })),
        transition('inactive => active', animate('100ms ease-in')),
        transition('active => inactive', animate('100ms ease-out'))
    ]);

export const slideInOutAnimation =
    // trigger name for attaching this animation to an element using the [@triggerName] syntax
    trigger('slideInOutAnimation', [
 
        
 
        // route 'enter' transition
        transition(':enter', [
 
            // styles at start of transition
            style({
                // start with the content positioned off the right of the screen,
                // -400% is required instead of -100% because the negative position adds to the width of the element
                right: '-400%',
 
                // start with background opacity set to 0 (invisible)
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }),
 
            // animation and styles at end of transition
            animate('.5s ease-in-out', style({
                // transition the right position to 0 which slides the content into view
                right: 0,
 
                // transition the background opacity to 0.8 to fade it in
                backgroundColor: 'rgba(0, 0, 0, 0.8)'
            }))
        ]),
 
        // route 'leave' transition
        transition(':leave', [
            // animation and styles at end of transition
            animate('.5s ease-in-out', style({
                // transition the right position to -400% which slides the content out of view
                right: '-400%',
 
                // transition the background opacity to 0 to fade it out
                backgroundColor: 'rgba(0, 0, 0, 0)'
            }))
        ])
    ]);

    export const flyInOut = trigger('flyInOut', [
        state('in', style({width: 120, transform: 'translateX(0)', opacity: 1})),
        transition('in => out', [
        style({width: 10, transform: 'translateX(50px)', opacity: 0}),
        group([
            animate('0.3s 0.1s ease', style({
            transform: 'translateX(0)',
            width: 120
            })),
            animate('0.3s ease', style({
            opacity: 1
            }))
        ])
        ]),
        transition('out => in', [
        group([
            animate('0.3s ease', style({
            transform: 'translateX(50px)',
            width: 10
            })),
            animate('0.3s 0.2s ease', style({
            opacity: 0
            }))
        ])
        ])
    ]);