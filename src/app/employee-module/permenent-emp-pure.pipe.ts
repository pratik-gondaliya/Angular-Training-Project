import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from './employee.pojo';
@Pipe({
  name: 'permenentEmpPure'
})
export class PermenentEmpPurePipe implements PipeTransform {

  transform(employee: Employee[], args?: any): any {
    return employee.filter(emp=>emp.isPermenent);
  }

}
