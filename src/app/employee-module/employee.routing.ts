import {RouterModule,Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import { EmployeeListComponent } from './employee-list/employee-list.component';

const routes:Routes = [
    {
        path:'employees',
        component: EmployeeListComponent
    }
];

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class EmployeeRoutingmodule{};
