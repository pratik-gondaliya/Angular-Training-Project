import { Component, OnInit,Input , Output , EventEmitter} from '@angular/core';
import { Employee } from '../employee.pojo';
import { ParentChildCommunicatorService } from '../parent-child-communicator.service';
@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  @Input() empObj:Employee;
  @Output() onMergeName = new EventEmitter();
  public empFullName:String;
  constructor(private parentChildCommunicatorService:ParentChildCommunicatorService) { 
    
  }

  ngOnInit() {
    this.parentChildCommunicatorService.notifyObservable$.subscribe((res)=>{
        alert(res.data);
    });
  }
  
  mergeName() {
    this.empFullName = this.empObj.firstName + " " +  this.empObj.lastName;
    this.onMergeName.emit(this.empFullName + " Merge SuccessFully");
  }
}
