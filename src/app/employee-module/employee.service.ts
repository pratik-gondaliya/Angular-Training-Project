import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpClient} from '@angular/common/http';

import {EmployeeData} from '../common/employee.interface';
import { environment } from '../../environments/environment'; 
@Injectable()
export class EmployeeService {
  constructor(private httpClient:HttpClient,private http:Http) { }

  getRequest() {
    console.log('get request called');
    return this.httpClient.get<EmployeeData>(environment.baseUrl);
  }

  getNonJsonResponse() {
    return this.http.get("https://api.github.com/users/pratik-gondaliya").map((response:Response)=>{
      return response.text();
    });
  }
}
