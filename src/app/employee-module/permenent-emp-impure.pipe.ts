import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from './employee.pojo';
@Pipe({
  name: 'permenentEmpImpure',
  pure: false
})
export class PermenentEmpImpurePipe implements PipeTransform {

 transform(employee: Employee[], args?: any): any {
    return employee.filter(emp=>emp.isPermenent);
}

}
