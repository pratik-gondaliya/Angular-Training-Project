import { Component,ViewChildren ,OnInit , OnChanges,QueryList} from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Employee } from "../employee.pojo";
import {EmployeeComponent} from '../employee/employee.component';
import {ParentChildCommunicatorService} from '../parent-child-communicator.service';
import {EmployeeService} from '../employee.service';
import {slideInOutAnimation,heroState,flyInOut} from '../employee.animation';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
  animations:[heroState,slideInOutAnimation,flyInOut],
})
export class EmployeeListComponent implements OnInit,OnChanges {

  @ViewChildren(EmployeeComponent) empolyeeComponentList : QueryList<EmployeeComponent>;
  public emparr = [];
  perEmp = new Employee();
  emp = new Employee();
  color: string;
  jsonResponse:string;
  nonJsonResponse:string;
  today:Date;
  empName:string;
  isPermenent:boolean = true;
  constructor(private parentChildCommunicatorService:ParentChildCommunicatorService,private employeeService:EmployeeService) {
     console.log('Employee list constructor called');
     this.today = new Date();
      this.employeeService.getRequest().subscribe(data=>{
        this.jsonResponse = JSON.stringify(data);
      },(error:HttpErrorResponse)=>{
        console.log("error occured" + error.message);
      });

      this.employeeService.getNonJsonResponse().subscribe(data=>{
        this.nonJsonResponse = data;
      });
  }

  addEmployee() {
    this.empName = this.empName.trim();
    let firstName = this.empName.split(' ')[0];
    let lastName = this.empName.split(' ')[1];
    let tempEmp = new Employee(firstName,lastName,this.isPermenent);
    this.emparr.push(tempEmp);
    this.empName = "";
  }

  get format()   { return 'dd/MMM/yyyy' }
  //get format()   { return 'shortDate' }
  //get format()   { return 'fullDate' }

  ngOnInit(){
     this.perEmp.firstName = "Permenent";
     this.perEmp.lastName = "Employee";
     this.perEmp.isPermenent = true;
     this.emparr.push(this.perEmp);

     
  }

  ngOnChanges() {
    for(let employeeComponent of this.empolyeeComponentList.toArray()) {
        employeeComponent.empFullName = "Click Merge button show full name";
      }
    
  }

  onMergeClick() {
  
    for(let employeeComponent of this.empolyeeComponentList.toArray()) {
        employeeComponent.mergeName();
    }
    
  }

  onMergeName(mergeName:String) {
    alert("Name merged : " + mergeName);
  }

  onCallChildFromParentClick() {
    this.parentChildCommunicatorService.notifyOther({data:'data pass from the parent to child using the service'})
  }

  name:string;
  state:string = 'out';
  animationStarted(event:any) {
    this.name=' ';

  }
  animationDone(event:any) {
    this.name= 'click me to animate';
  }

  toggleState() {
    this.state = this.state === 'in' ? 'out':'in'; 
  }
}