import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { EmployeeComponent } from './employee/employee.component';
import { ParentChildCommunicatorService } from './parent-child-communicator.service';
import { EmployeeService } from './employee.service';
import { HighlightDirective } from './highlight.directive';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeRoutingmodule } from './employee.routing';
import { HttpClientModule } from '@angular/common/http';
import { PowerPipe } from './power.pipe';
import { PermenentEmpPurePipe } from './permenent-emp-pure.pipe';
import { PermenentEmpImpurePipe } from './permenent-emp-impure.pipe';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    EmployeeRoutingmodule
  ],
  declarations: [
    EmployeeComponent,
    HighlightDirective,
    EmployeeListComponent,
    PowerPipe,
    PermenentEmpPurePipe,
    PermenentEmpImpurePipe
  ],
  // For use in another module in which this component added
  exports: [
    EmployeeComponent,
    HighlightDirective
  ],
  providers:[ParentChildCommunicatorService,EmployeeService]
})
export class EmployeeModuleModule { 
  static forRoot() : ModuleWithProviders {
    return {
      ngModule:EmployeeModuleModule,
      providers:[ParentChildCommunicatorService]
    }
  }
}
