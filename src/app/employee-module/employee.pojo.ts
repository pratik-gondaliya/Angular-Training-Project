export class Employee {
    public state:string = 'inactive';
    constructor(public firstName?: string, public lastName?: string,public isPermenent?:boolean) { 

    }

  toggleState() {
    this.state = this.state === 'active' ? 'inactive' : 'active';
  }
} 