import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'power'
})
export class PowerPipe implements PipeTransform {

  transform(value: any, powerval?: number): any {
    return Math.pow(value, isNaN(powerval) ? 1 : powerval);
  }

}
