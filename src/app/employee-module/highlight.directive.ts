import { Directive,ElementRef,Input,HostListener,OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit{
  @Input('color') color:String;
  constructor(private el : ElementRef) { }

  @HostListener('mouseenter') mouseEnter(){
      this.highlighColor(this.color);
  }

  @HostListener('mouseleave') mouseLeave(){
    this.highlighColor(null);
  }

  highlighColor(color:String) {
    this.el.nativeElement.style.backgroundColor = color;
  }

  ngOnInit() {
    this.color = 'red';
  }
}
